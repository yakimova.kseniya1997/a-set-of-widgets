class CustomHeader extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open" });
        this.shadowRoot.append(CustomHeader.template.content.cloneNode(true));
        this.timeElement = this.shadowRoot.querySelector('#time');
        this.menuButton = this.shadowRoot.querySelector('button');
        this.dropdown = this.shadowRoot.querySelector('#dropdown');

        this.timeUpdate();
        this.menuButton.addEventListener('click', () => this.dropdown.classList.toggle("show"));
    }

    timeUpdate() {
        setInterval(() => {
            let date = new Date();
            this.timeElement.innerHTML = (date.getHours().toString().padStart(2, '0') + ":" + date.getMinutes().toString().padStart(2, '0') + ":" + date.getSeconds().toString().padStart(2, '0'));
        }, 1000);
    }
}

CustomHeader.template = document.createElement('template');

CustomHeader.template.innerHTML = `
<style>
header {
    float: right;
    background-color: #2F3A58;
    height:56px;
    width: 100%;
    margin: 0;
    display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;    
}

.general_name {
font-family: 'Roboto', sans-serif;
font-size: 24px;
font-weight: 400;
line-height: 28px;
letter-spacing: 0em;
text-align: left;
color: #FFFFFF;
margin-left: 17px;
flex: 1;
}

.logo {
    height: 33px;
    width: 33px;
    margin-left: 16%;
}

.menu {
    margin-right: 16%;
    position: relative;
    display: inline-block;
}

.time {
    height: 28px;
    width: 93px;
    margin-right: 23px;
    margin-left: 238px;
    font-family: 'Roboto', sans-serif;
font-size: 24px;
font-weight: 400;
line-height: 28px;
letter-spacing: 0em;
text-align: left;
color: #FFFFFF;
}

button {
    background: transparent;
    border: 0;
    cursor: pointer;
}

.dr-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
    right: 0;
    margin-top: 5px;
}

.dr-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    border: #000000;
}

.dr-content a:hover {
    background-color: rgb(145, 141, 141)
}

.show {
    display: block;
}

.strelochka {
    position: absolute;
    top: -5px;
    right: 10px;
  }

@media (max-width: 400px) {
    .general_name {
        display: none;
    }

    .time {
        margin-left: 25%;
    }


}
</style>
<header>
<div class="logo">
<img src="logo.svg">
</div>
<div class="general_name">Виджеты</div>
<div id="time" class="time"></div>
<div class="menu">
<button><img src="Vector.svg" alt=""></button>
<div id="dropdown" class="dr-content">
    <img class="strelochka" src="Vector(1).svg">
    <a href="#">Главная</a>
    <a href="#">Погода</a>
    <a href="#">Список дел</a>
</div>
</div>
</hearder>
`;

customElements.define('custom-header', CustomHeader);
