class CustomFooter extends HTMLElement { 
    constructor()  {
        super(); 
        this.attachShadow({mode: "open"});
        this.shadowRoot.append(CustomFooter.template.content.cloneNode(true));
    }
}

CustomFooter.template = document.createElement('template');

CustomFooter.template.innerHTML = `
<style>
    footer {
        background-color: #F6F8FA;
        height: 68px;
        width: 100%;
        margin: 0;
        display: flex;
        flex-flow: row nowrap;
        justify-content: space-between;
        align-items: center;
        height: 68px;
        left: 0px;
        border-radius: 0px;
        margin-top: auto;
    }

    .about_link {
        margin-left: 16%;
    }
    
    .about_link a {
        color: #000000;
    }
</style>
<footer>
    <div class="about_link">
        <a href="about/index.html">О сайте</a>
    </div>
</footer>
`;
customElements.define('custom-footer', CustomFooter);
